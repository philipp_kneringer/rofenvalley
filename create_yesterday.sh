#!/bin/bash

YESTERDAY=`/bin/date -d "1 days ago" +"%Y-%m-%d"`
/opt/R-3.1.1/bin/Rscript /mnt/imgi2-a/c7071039/rofenvalley/rofenvalley.R --datum="${YESTERDAY} 00:00:00" --archive=T

