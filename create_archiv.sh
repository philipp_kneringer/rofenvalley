#!/bin/bash

date=${1:-20170828} #give day before real startday
end_date=${2:-20180114} #real end day

while [[ $date < $end_date ]]
do
    #echo $date not reached $end_date
    date=$(date --date="$date + 1 days" +'%Y-%m-%d')
    echo "\\n============================================================== GENERATING DATE: "${date}"\\n"
    #echo ${date}
    /opt/R-3.1.1/bin/Rscript rofenvalley.R --datum="${date} 00:00:00" --archive=T
done
