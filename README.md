# rofenvalley

Scripts to visualize ACINN measurements from Rofen Valley

rofenvalley.R:
	Main script to produce the png's by using the command

	Rscript rofenvalley.R --datum=<date time string> --archive=<T or F>
		
		--datum		end of the plot, if not set date/time is set to system now
		--archive	if T png is in archive form

	Example:
	Rscript rofenvalley.R --datum="2018-02-27 00:00:00" --archive=T


config.conf:
	Take config_example.conf modify and rename it to config.conf.
	Config file called by rofenvalley.R 
	
	set path to data files
	set path to output files
	set temporal resolution of visualized measurements (10, 30, 60 min)
	set window width in days


loaddata.R:
	Script loading needed R packages, defining functions and parameters,
	and loading the data. Script is called by rofenvalley.R.
